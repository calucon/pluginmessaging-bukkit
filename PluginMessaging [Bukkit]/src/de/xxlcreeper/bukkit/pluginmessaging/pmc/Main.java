package de.xxlcreeper.bukkit.pluginmessaging.pmc;

import org.bukkit.plugin.java.JavaPlugin;

public class Main{

	protected static JavaPlugin pl;
	protected static PluginMessageReceiver receiver;
	private boolean enabled = false;

	public Main(JavaPlugin pl){
		if(!enabled){
			Main.pl = pl;
			receiver = new PluginMessageReceiver();
			PluginMessageReceiver.registerChannel(PluginMessageDebug.DEBUG_CHANNEL);
			PluginMessageReceiver.setNonUnregisterable(PluginMessageDebug.DEBUG_CHANNEL);
			PluginMessageReceiver.registerChannel(PacketFactory.SPLIT_PACKET_NOTIFY_CHANNEL);
			PluginMessageReceiver.setNonUnregisterable(PacketFactory.SPLIT_PACKET_NOTIFY_CHANNEL);	
			this.enabled = true;
		}		
	}

}