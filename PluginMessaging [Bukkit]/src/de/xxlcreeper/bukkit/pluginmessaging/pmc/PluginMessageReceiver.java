package de.xxlcreeper.bukkit.pluginmessaging.pmc;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class PluginMessageReceiver implements PluginMessageListener{

	private static ArrayList<String> in_subscribed = new ArrayList<>();
	private static ArrayList<String> non_unregisterable =  new ArrayList<>();
	protected PluginMessageReceiver(){}
	
//------------------------------------------	
	
	/**
	 * 
	 * @param channel
	 * 		The Plugin will start to listen for messages on the selected channel
	 */
	public static void registerChannel(String channel){
		if(!PluginMessageReceiver.in_subscribed.contains(channel)){
			Main.pl.getServer().getMessenger().registerIncomingPluginChannel(Main.pl, channel, Main.receiver);
			PluginMessageReceiver.in_subscribed.add(channel);
			System.out.println("[PluginMessage] Listening to channel '" + channel + "'");
		}
	}
	
	/**
	 * 
	 * @param channel
	 * The Plugin will stop listening for messages on the selected channel
	 */
	public static void unregisterChannel(String channel){
		if(PluginMessageReceiver.in_subscribed.contains(channel) && !PluginMessageReceiver.non_unregisterable.contains(channel)){
			Main.pl.getServer().getMessenger().unregisterIncomingPluginChannel(Main.pl, channel);
			PluginMessageReceiver.in_subscribed.remove(channel);
			System.out.println("[PluginMessage] Stopped listening to channel '" + channel + "'");
		}
	}
		
//------------------------------------------
	
	/**
	 * 
	 * @param channel
	 * Channel that can't be unregistered anymore until the Plugin stops
	 * @return unregisterable status set
	 */
	public static boolean setNonUnregisterable(String channel){
		if(PluginMessageReceiver.in_subscribed.contains(channel) && !PluginMessageReceiver.non_unregisterable.contains(channel)){
			return PluginMessageReceiver.non_unregisterable.add(channel);
		}
		return false;
	}
	
	/**
	 * 
	 * @return List of channels the Plugin is listening to
	 */
	public static ArrayList<String> getSubscribed(){
		return new ArrayList<>(PluginMessageReceiver.in_subscribed);
	}
	
//------------------------------------------
	
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if(!PacketFactory.check(channel, message, player)){
			PluginMessageDebug.check(channel, message);		
			if(PluginMessageReceiver.in_subscribed.contains(channel) && !channel.equals(PluginMessageDebug.DEBUG_CHANNEL)){
				Main.pl.getServer().getPluginManager().callEvent(new PluginMessageReceiveEvent(new PluginMessage(channel, message, player)));
			}	
		}
	}
	
}
