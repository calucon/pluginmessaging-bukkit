package de.xxlcreeper.bukkit.pluginmessaging.pmc;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UTFDataFormatException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.entity.Player;

public class PluginMessageSender {

	private static ArrayList<String> out_subscribed = new ArrayList<>();
	private PluginMessageSender(){}

//------------------------------------------
	
	/**
	 * 
	 * @param message The message
	 * @param channel The channel in which the message will be sent
	 * @param player The sender of the Message
	 */
	public static void send(String message, String channel, Player player){
		send(Arrays.asList(new String[]{message}), channel, player);
	}
	
	/**
	 * 
	 * @param messages List of messages
	 * @param channel The channel in which the message will be sent
	 * @param player The sender of the Message
	 */
	public static void send(Collection<? extends String> messages, final String channel, Player player){
		isRegistered(channel);
		byte[] outgoing;
		
		if(PluginMessageDebug.isEnabled() && !channel.equals(PluginMessageDebug.DEBUG_CHANNEL)){
			isRegistered(PluginMessageDebug.DEBUG_CHANNEL);
			outgoing = packet(PluginMessageDebug.getAwait(messages));
			
			if(outgoing == null){
				PacketFactory.sendSplitPackets(messages, channel, player);
			} else {
				if(pass(outgoing)) {
					player.sendPluginMessage(Main.pl, channel, outgoing);
					
					byte[] debugPacket = packet(PluginMessageDebug.addChannel(messages, channel));
					/*packet(Arrays.asList(new String[]{channel, String.valueOf(messages.hashCode())}));*/
					final PluginMessage msg = new PluginMessage(PluginMessageDebug.DEBUG_CHANNEL, debugPacket, null);
					PluginMessageDebug.messages.add(msg);
					player.sendPluginMessage(Main.pl, PluginMessageDebug.DEBUG_CHANNEL, debugPacket);
					
					new Timer().schedule(new TimerTask() {				
						@Override
						public void run() {
							if(PluginMessageDebug.messages.contains(msg)){
								System.err.println("[PluginMessage] The Bungee Server probably did not receive your packet in the channel: '" + channel + "'. Make sure the Bungee is listening to that channel!");
								PluginMessageDebug.messages.remove(msg);
							}
						}
					}, PluginMessageDebug.delay);
				} else {
					PacketFactory.sendSplitPackets(messages, channel, player);
				}
			}
		} else {
			outgoing = packet(messages);
			if(outgoing == null){
				PacketFactory.sendSplitPackets(messages, channel, player);
			} else {
				if(pass(outgoing)) {
					player.sendPluginMessage(Main.pl, channel, outgoing);
				} else if (!channel.equals(PluginMessageDebug.DEBUG_CHANNEL)){
					PacketFactory.sendSplitPackets(messages, channel, player);
				}	
			}
		}
	}
	
//------------------------------------------
	
	/**
	 * 
	 * @return List of used outgoing channels#
	 * Bukkit required you to register outgoing channels as well, but the plugin takes care of that
	 */
	public static ArrayList<String> getSubscribed(){
		return new ArrayList<>(PluginMessageSender.out_subscribed);
	}
	
//------------------------------------------
	
	private static boolean pass(byte[] outgoing){
		return (outgoing.length < PacketFactory.maxLength);
	}
	
	private static void isRegistered(String channel){
		if(!PluginMessageSender.out_subscribed.contains(channel)){
			Main.pl.getServer().getMessenger().registerOutgoingPluginChannel(Main.pl, channel);
		}
	}
	
	protected static byte[] packet(Collection<? extends String> messages){
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bytes);
		try {
			for(String msg : messages){
				out.writeUTF(msg);
			}
		} catch (IOException e) {			
			if(e instanceof UTFDataFormatException){
				return null;
			} else {
				e.printStackTrace();
			}
		}
		return bytes.toByteArray();
	}
	
}
