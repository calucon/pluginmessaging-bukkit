package de.xxlcreeper.bukkit.pluginmessaging.pmc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

public class PluginMessage {

	private String channel;
	private byte[] message;
	private ArrayList<String> response;
	private Player player;
	private boolean AWAIT = false;
	
	protected PluginMessage(String ch, byte[] msg, Player player){
		this(ch, msg, player, false);
	}
	
	protected PluginMessage(String ch, byte[] msg, Player player, boolean isLocal){		
		this.channel = ch;
		this.message = msg;
		this.player = player;
		this.response = new ArrayList<>();
		
		if(isLocal){
			response = new ArrayList<>(Arrays.asList(new String(msg).split(Pattern.quote(PacketFactory.PATTERN_NEXT))));
		} else {
			try{
				ByteArrayDataInput in = ByteStreams.newDataInput(message);
				String str = "";
				while((str = in.readUTF()) != null){
					str = str.trim();
					if(str.equals(PluginMessageDebug.KEY_AWAIT)){
						AWAIT = true;
					} else {
						if(!str.equals("")){
							response.add(str);
						}
					}
				}	
			} catch(Exception e){}
			
			if(AWAIT == true){
				ArrayList<String> arr = new ArrayList<>(response);
				arr.add(0, channel);
				PluginMessageSender.send(arr, PluginMessageDebug.DEBUG_CHANNEL, player);
			}	
		}
	}
	
//------------------------------------------
	
	public Player getPlayer(){
		return this.player;
	}
	
	/**
	 * 
	 * @return Name of the Channel the Packet was sent in
	 */
	public String getChannel(){
		return this.channel;
	}
	
	/**
	 * 
	 * @return The original, no processed byte array
	 */
	public byte[] getMessageAsBytes(){
		return this.message;
	}
	/**
	 * 
	 * @return The original, no processed byte array as String
	 */
	public String getMessageAsString(){
		return new String(message);
	}
	/**
	 * 
	 * @param i line
	 * @return The x. line of this PluginMessage
	 */
	public String getMessage(int i){
		return this.response.get(i);
	}
	/**
	 * 
	 * @return ArrayList containing all Messages in this PluginMessage
	 */
	public ArrayList<String> getMessage(){
		return this.response;
	}
	
}
