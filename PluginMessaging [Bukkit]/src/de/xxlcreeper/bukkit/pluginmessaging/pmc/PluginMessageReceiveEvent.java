package de.xxlcreeper.bukkit.pluginmessaging.pmc;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import de.xxlcreeper.bukkit.pluginmessaging.pmc.PluginMessage;

public class PluginMessageReceiveEvent extends Event{

	private PluginMessage message;
	
	public PluginMessageReceiveEvent(PluginMessage m){
		this.message = m;
	}
	
//------------------------------------------
	
	public PluginMessage message(){
		return this.message;
	}
	
	public Player getPlayer(){
		return this.getPlayer();
	}
	
	/**
	 * 
	 * @return Name of the Channel the Packet was sent in
	 */
	public String getChannel(){
		return this.message.getChannel();
	}
	
	/**
	 * 
	 * @return The original, no processed byte array
	 */
	public byte[] getMessageAsBytes(){
		return this.message.getMessageAsBytes();
	}
	/**
	 * 
	 * @return The original, no processed byte array as String
	 */
	public String getMessageAsString(){
		return this.message.getMessageAsString();
	}
	/**
	 * 
	 * @param i line
	 * @return The x. line of this PluginMessage
	 */
	public String getMessage(int i){
		return this.message.getMessage(i);
	}
	/**
	 * 
	 * @return ArrayList containing all Messages in this PluginMessage
	 */
	public ArrayList<String> getMessage(){
		return this.message.getMessage();
	}

	
//-----------------------------------------------------------------------	
	
	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
	    return handlers;
	}
			
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
}
