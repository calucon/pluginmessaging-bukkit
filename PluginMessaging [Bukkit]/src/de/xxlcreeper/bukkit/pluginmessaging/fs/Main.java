package de.xxlcreeper.bukkit.pluginmessaging.fs;

import java.io.File;
import java.nio.file.Paths;

import org.bukkit.plugin.java.JavaPlugin;

public class Main{
	
	protected static String PREFIX_NOT_READY = "NOT_READY_";
	protected static String CHANNEL_PATH;
	protected static String SERVER_PATH;
	protected static String PATH;
	protected static JavaPlugin pl;
	protected static boolean running = true;
	
	public Main(JavaPlugin pl){
		Main.pl = pl;
		PATH = System.getProperty("java.io.tmpdir") + "PluginMessaging";
		CHANNEL_PATH = Paths.get(PATH, String.valueOf(pl.getServer().getPort())).toString();
		SERVER_PATH = Paths.get(PATH, "Serverlist").toString();
		new File(CHANNEL_PATH).mkdirs();	
		new File(SERVER_PATH).mkdirs();
		
		new Messenger();
		new PluginMessageReceiver().thread.start();
		new PluginMessageSender().thread.start();
	}
	
	/**
	 * disables the IOService
	 */
	public void disable(){
		Main.running = false;
		Messenger.unregister();
		for(File f : new File(CHANNEL_PATH).listFiles()){
			while(!f.delete());
		}
		System.gc();
	}
	
}
