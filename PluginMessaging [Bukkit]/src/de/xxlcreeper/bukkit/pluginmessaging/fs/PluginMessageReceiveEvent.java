package de.xxlcreeper.bukkit.pluginmessaging.fs;

import java.util.ArrayList;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PluginMessageReceiveEvent extends Event{

	private String ch;
	private PluginMessage msg;
	
	protected PluginMessageReceiveEvent(String channel, PluginMessage message){
		this.ch = channel;
		this.msg = message;
	}
	
//------------------------------------------	
	
	public PluginMessage message(){
		return this.msg;
	}
	
	/**
	 * 
	 * @return Name of the Channel the Packet was sent in
	 */
	public String getChannel(){
		return this.ch;
	}
	
	/**
	 * 
	 * @param i line
	 * @return The x. line of this PluginMessage
	 */
	public String getMessage(int i){
		return this.msg.getMessage(i);
	}
	
	/**
	 * 
	 * @return ArrayList containing all Messages in this PluginMessage
	 */
	public ArrayList<String> getMessages(){
		return this.msg.getMessages();
	}
	
//-----------------------------------------------------------------------	
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
				
	public static HandlerList getHandlerList() {
	    return handlers;
	}
		
}
