package de.xxlcreeper.bukkit;

import org.bukkit.plugin.java.JavaPlugin;

public class PluginMessenger extends JavaPlugin{

	de.xxlcreeper.bukkit.pluginmessaging.fs.Main fs;
	
	@Override
	public void onEnable() {
		new de.xxlcreeper.bukkit.pluginmessaging.pmc.Main(this);
		fs = new de.xxlcreeper.bukkit.pluginmessaging.fs.Main(this);
	}

	@Override
	public void onDisable() {
		fs.disable();
	}
	
}
